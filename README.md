# README #

### What is this repository for? ###

This repository contains all reusable collateral for Azure Services of the BHP Mobility Middleware Platform.
The key materials hosted here are:

* Azure Policy Definitions for Governance 
* Azure Resource Manager Templates for reusability 

### Who do I talk to? ###

David.lee@bhpbilliton.com
